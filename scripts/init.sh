#!/bin/bash

export PROJECT_ID="45782563" # CHANGE TO YOURS
export TF_USERNAME="${GITLAB_USERNAME}" ## Or use Vault after bootsrapped # =$(vault kv get -field=tf_username kv/matrix/tf_credentials)
export TF_PASSWORD="${GITLAB_TOKEN}"    ## or use Vault after bootsrapped # =$(vault kv get -field=tf_password kv/matrix/tf_credentials)
export STATE_NAME="construct"
export TF_ADDRESS="https://gitlab.com/api/v4/projects/${PROJECT_ID}/terraform/state/${STATE_NAME}"

if [[ -z ${TF_USERNAME} ]]; then
  read -p "What is your Gitlab Username? " $TF_USERNAME
fi 

if [[ -z ${TF_PASSWORD} ]]; then
  read -p "What is your Gitlab Token? " $TF_PASSWORD
fi 


terraform init $@ \
  -backend-config=address=${TF_ADDRESS} \
  -backend-config=lock_address=${TF_ADDRESS}/lock \
  -backend-config=unlock_address=${TF_ADDRESS}/lock \
  -backend-config=username=${TF_USERNAME} \
  -backend-config=password=${TF_PASSWORD} \
  -backend-config=lock_method=POST \
  -backend-config=unlock_method=DELETE \
  -backend-config=retry_wait_min=5
