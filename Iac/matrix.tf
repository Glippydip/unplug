module "servers" {
  source = "./modules/servers"
  providers = {
    proxmox = proxmox
  }

  standalone_server = local.standalone_server

  matrix_user     = local.matrix_user
  matrix_password = local.matrix_password
  vlan_tag        = local.vlan_tag
  vlan_cidr       = local.vlan_cidr
  vlan_gateway    = local.vlan_gateway
  vlan_nameserver = local.vlan_nameserver
}

module "dns" {
  source = "./modules/dns"
  providers = {
    proxmox = proxmox
  }
  count = local.standalone_server ? 0 : 1

  matrix_user     = local.matrix_user
  matrix_password = local.matrix_password
  vlan_tag        = local.vlan_tag
  vlan_cidr       = local.vlan_cidr
  vlan_gateway    = local.vlan_gateway
  vlan_nameserver = local.vlan_nameserver

  depends_on = [module.servers]
}

module "loadbalancers" {
  source = "./modules/loadbalancers"
  providers = {
    proxmox = proxmox
  }
  count = local.standalone_server ? 0 : 1

  matrix_user     = local.matrix_user
  matrix_password = local.matrix_password
  vlan_tag        = local.vlan_tag
  vlan_cidr       = local.vlan_cidr
  vlan_gateway    = local.vlan_gateway
  vlan_nameserver = local.vlan_nameserver

  depends_on = [module.servers]

}
