variable "matrix_password" {
  type      = string
  sensitive = true
}

variable "matrix_user" {
  type      = string
  sensitive = true
  default   = "neo"
}

variable "proxmox_api_url" {
  type = string
}

variable "proxmox_api_token_id" {
  type      = string
  sensitive = true
}

variable "proxmox_api_token_secret" {
  type      = string
  sensitive = true
}

locals {
  standalone_server = false
  public_keys       = file("./public_keys")
  matrix_user       = var.matrix_user
  matrix_password   = var.matrix_password
  vlan_tag          = 4
  vlan_gateway      = "10.10.4.1"
  vlan_cidr         = "/24"
  vlan_nameserver   = "10.10.4.1"


}

provider "proxmox" {
  pm_api_url          = var.proxmox_api_url
  pm_api_token_id     = var.proxmox_api_token_id
  pm_api_token_secret = var.proxmox_api_token_secret
  pm_tls_insecure     = true
}
