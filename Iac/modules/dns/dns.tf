
module "pihole" {
  source = "../matrix-server"
  providers = {
    proxmox = proxmox
  }
  vim_id            = 102
  name              = "pihole"
  target_node       = "proxmox"
  vm_desc           = "pihole server"
  matrix_user       = var.matrix_user
  matrix_password   = var.matrix_password
  sshKeys           = var.sshKeys 
  on_boot           = true
  cores             = 2
  memory            = 2048
  disk_size         = "55G"
  storage           = "local-lvm"
  bridge            = "vmbr1"
  server_ip         = "10.10.4.10"
  vlan_tag          = var.vlan_tag
  CIDR              = var.vlan_cidr
  server_gateway    = var.vlan_gateway
  server_nameserver = var.vlan_nameserver

}

module "pihole_2" {
  source = "../matrix-server"
  providers = {
    proxmox = proxmox
  }
  vim_id            = 103
  name              = "pihole-2"
  target_node       = "proxmox"
  vm_desc           = "pihole-2 server"
  matrix_user       = var.matrix_user
  matrix_password   = var.matrix_password
  sshKeys           = var.sshKeys
  on_boot           = true
  cores             = 2
  memory            = 2048
  disk_size         = "55G"
  storage           = "local-lvm"
  bridge            = "vmbr1"
  server_ip         = "10.10.4.11"
  vlan_tag          = var.vlan_tag
  CIDR              = var.vlan_cidr
  server_gateway    = var.vlan_gateway
  server_nameserver = var.vlan_nameserver

}