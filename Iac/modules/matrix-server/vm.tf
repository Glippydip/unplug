resource "proxmox_vm_qemu" "server" {
  vmid        = var.vim_id
  name        = var.name
  target_node = var.target_node
  desc        = var.vm_desc
  os_type     = "cloud-init"
  ciuser      = var.matrix_user
  cipassword  = var.matrix_password
  sshkeys     = var.sshKeys
  clone       = "matrix-server"
  onboot      = var.on_boot
  agent       = 1
  cores       = var.cores
  sockets     = 1
  cpu         = "host"
  memory      = var.memory
  balloon     = 1024
  disk {
    backup             = 0
    cache              = "none"
    format             = "raw"
    iops               = 0
    iops_max           = 0
    iops_max_length    = 0
    iops_rd            = 0
    iops_rd_max        = 0
    iops_rd_max_length = 0
    iops_wr            = 0
    iops_wr_max        = 0
    iops_wr_max_length = 0
    iothread           = 0
    mbps               = 0
    mbps_rd            = 0
    mbps_rd_max        = 0
    mbps_wr            = 0
    mbps_wr_max        = 0
    replicate          = 0
    size               = var.disk_size
    slot               = 0
    ssd                = 0
    storage            = var.storage
    type               = "virtio"
  }
  network {
    bridge = var.bridge
    model  = "virtio"
    tag    = var.vlan_tag
  }
  ipconfig0  = "ip=${var.server_ip}${var.CIDR},gw=${var.server_gateway}"
  nameserver = var.server_nameserver
}

# resource "time_sleep" "wait_30_seconds" {
#   create_duration = "30s"
#   depends_on = [ proxmox_vm_qemu.server ]
# }


# resource "ansible_playbook" "configure" {
#   ansible_playbook_binary = "ansible-playbook"
#   playbook                = "modules/matrix-server/configure.yml"
#   replayable = true
#   # inventory configuration
#   name = var.server_ip
#   limit = [
#     var.server_ip
#   ]
#   check_mode = false
#   diff_mode  = false
#   force_handlers = false
#   ignore_playbook_failure = false
#   # play control
#   #var_files = [
#   #  "var-file.yml"
#   #]
#   verbosity = 6
#   # connection configuration and other vars
#   extra_vars = {
#     ansible_hostname   = var.server_ip
#     ansible_host = var.server_ip
#     ansible_user = var.matrix_user
#     ansible_password = var.matrix_password
#     ansible_private_key_file = var.ansible_private_key_file
#     DISABLE_STUB = false
#     REBOOT = true
#   }

#   depends_on = [ time_sleep.wait_30_seconds ]

# }
