terraform {
  required_providers {
    # vault = {
    #   source  = "hashicorp/vault"
    #   version = "3.14.0"
    # }
    proxmox = {
      source  = "telmate/proxmox",
      version = "2.9.11"
    }
    # ansible = {
    #   version = "~> 1.0.0"
    #   source  = "ansible/ansible"
    # }
    # time = {
    #   source = "hashicorp/time"
    #   version = "0.9.1"
    # }
  }
}

variable "vim_id" {
  type = number
  description = "The VM ID to assign to the Proxmox virtual machine"
}

variable "name" {
  type = string
  description = "The name of the Proxmox virtual machine"
}

variable "target_node" {
  type = string
  description = "The name of the Proxmox node where the virtual machine will be created"
}

variable "vm_desc" {
  type = string
  description = "The description of the Proxmox virtual machine"
}

variable "matrix_user" {
  type = string
  description = "The username for the cloud-init user on the Proxmox virtual machine"
}

variable "matrix_password" {
  type      = string
  sensitive = true
  description = "The password for the cloud-init user on the Proxmox virtual machine"
}

variable "sshKeys" {
  type = string
  default = ""
  description = "A list of SSH public keys to add to the cloud-init user on the Proxmox virtual machine"
}


variable "on_boot" {
  type    = bool
  default = true
  description = "If set to true, the virtual machine will be started automatically on boot"
}

variable "cores" {
  type    = number
  default = 1
  description = "The number of CPU cores to allocate to the Proxmox virtual machine"
}

variable "memory" {
  type    = number
  default = 1024
  description = "The amount of memory to allocate to the Proxmox virtual machine, in megabytes"
}

variable "disk_size" {
  type    = string
  default = "40G"
  description = "The size of the virtual disk to create for the Proxmox virtual machine, in gigabytes 40G"
}

variable "storage" {
  type = string
  description = "The name of the Proxmox storage to use for the virtual machine's virtual disk"
  default = "local-lvm"
}

variable "bridge" {
  type = string
  description = "The name of the bridge to attach the virtual machine's network interface to"
  default = "vmbr1"
}

variable "vlan_tag" {
  type    = number
  default = 4
  description = "The VLAN tag to use for the virtual machine's network interface"
}

variable "server_ip" {
  type = string
  description = "The IP address to assign to the Pi-hole virtual machine"
}

variable "CIDR" {
  type = string
  description = "The IP address to CIDR /24"
  default = "/24"
}

variable "server_gateway" {
  type = string
  description = "The IP address of the gateway to use for the Pi-hole virtual machine"
}

variable "server_nameserver" {
  type = string
  description = "The IP address of the DNS server to use for the Pi-hole virtual machine"
}
