module "consul_mesh" {
  source = "../matrix-server"
  providers = {
    proxmox = proxmox
  }
  vim_id            = 106
  name              = "service-mesh"
  target_node       = "proxmox"
  vm_desc           = "service_mesh server"
  matrix_user       = var.matrix_user
  matrix_password   = var.matrix_password
  sshKeys           = var.sshKeys
  on_boot           = true
  cores             = 4
  memory            = 4096
  disk_size         = "100G"
  storage           = "local-lvm"
  bridge            = "vmbr2"
  server_ip         = "10.10.4.51"
  vlan_tag          = var.vlan_tag
  CIDR              = var.vlan_cidr
  server_gateway    = var.vlan_gateway
  server_nameserver = var.vlan_nameserver

  count = var.standalone_server ? 0 : 1

  depends_on = [module.construct]
}
