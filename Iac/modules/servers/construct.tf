module "construct" {
  source = "../matrix-server"
  providers = {
    proxmox = proxmox
  }
  vim_id            = 101
  name              = "construct-server"
  target_node       = "proxmox"
  vm_desc           = "container server server"
  matrix_user       = var.matrix_user
  matrix_password   = var.matrix_password
  sshKeys           = var.sshKeys
  on_boot           = true
  cores             = var.standalone_server ? 4 : 4
  memory            = var.standalone_server ? 8192 : 3072
  disk_size         = var.standalone_server ? "200G" : "80G"
  storage           = "local-lvm"
  bridge            = "vmbr1"
  server_ip         = "10.10.4.5"
  vlan_tag          = var.vlan_tag
  CIDR              = var.vlan_cidr
  server_gateway    = var.vlan_gateway
  server_nameserver = var.vlan_nameserver
}