module "backdoor" {
  source = "../matrix-server"
  providers = {
    proxmox = proxmox
  }
  vim_id            = 104
  name              = "backdoor"
  target_node       = "proxmox"
  vm_desc           = "backdoor server"
  matrix_user       = var.matrix_user
  matrix_password   = var.matrix_password
  sshKeys           = var.sshKeys
  on_boot           = true
  cores             =  2
  memory            = 2048
  disk_size         = "55G"
  storage           = "local-lvm"
  bridge            = "vmbr1"
  server_ip         = "10.10.4.2"
  vlan_tag          = var.vlan_tag
  CIDR              = var.vlan_cidr
  server_gateway    = var.vlan_gateway
  server_nameserver = var.vlan_nameserver

}




