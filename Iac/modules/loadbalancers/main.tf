terraform {
  required_providers {
    proxmox = {
      source  = "telmate/proxmox",
      version = "2.9.11"
    }
  }
}


variable "sshKeys" {
  type = string
  default = ""
  description = "A list of SSH public keys to add to the cloud-init user on the Proxmox virtual machine"
}
variable "vlan_tag" {
  type    = number
  default = 4
  description = "The VLAN tag to use for the virtual machine's network interface"
}

variable "vlan_cidr" {
  type = string
  description = "The vlan CIDR /24"
  default = "/24"
}

variable "vlan_gateway" {
  type = string
  description = "The IP address of the gateway to use for the vlan"
}

variable "vlan_nameserver" {
  type = string
  description = "The IP address of the DNS server to use for the vlan"
}

variable "matrix_password" {
  type      = string
  sensitive = true
}

variable "matrix_user" {
  type      = string
  sensitive = true
}